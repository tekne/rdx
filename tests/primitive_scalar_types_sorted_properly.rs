mod common;
use common::{sorts_properly, partial_sorts_properly};
use rdx::ska_sort::{ska_sort, ska_sort_by_key};
use rdx::float::{OrderedFloat, Float};

#[test]
fn small_u8_sorts_properly() { sorts_properly::<u8, _>(100, ska_sort) }

#[test]
fn medium_u8_sorts_properly() { sorts_properly::<u8, _>(20000, ska_sort) }

#[test]
fn large_u8_sorts_properly() { sorts_properly::<u8, _>(1000000, ska_sort) }

#[test]
fn small_u16_sorts_properly() { sorts_properly::<u16, _>(100, ska_sort) }

#[test]
fn medium_u16_sorts_properly() { sorts_properly::<u16, _>(20000, ska_sort) }

#[test]
fn large_u16_sorts_properly() { sorts_properly::<u16, _>(1000000, ska_sort) }


#[test]
fn small_u32_sorts_properly() { sorts_properly::<u32, _>(100, ska_sort) }

#[test]
fn medium_u32_sorts_properly() { sorts_properly::<u32, _>(20000, ska_sort) }

#[test]
fn large_u32_sorts_properly() { sorts_properly::<u32, _>(1000000, ska_sort) }


#[test]
fn small_u64_sorts_properly() { sorts_properly::<u64, _>(100, ska_sort) }

#[test]
fn medium_u64_sorts_properly() { sorts_properly::<u64, _>(20000, ska_sort) }

#[test]
fn large_u64_sorts_properly() { sorts_properly::<u64, _>(1000000, ska_sort) }

#[test]
fn small_usize_sorts_properly() { sorts_properly::<usize, _>(100, ska_sort) }

#[test]
fn medium_usize_sorts_properly() { sorts_properly::<usize, _>(20000, ska_sort) }

#[test]
fn large_usize_sorts_properly() { sorts_properly::<usize, _>(1000000, ska_sort) }


#[test]
fn small_i8_sorts_properly() { sorts_properly::<i8, _>(100, ska_sort) }

#[test]
fn medium_i8_sorts_properly() { sorts_properly::<i8, _>(20000, ska_sort) }

#[test]
fn large_i8_sorts_properly() { sorts_properly::<i8, _>(1000000, ska_sort) }

#[test]
fn small_i16_sorts_properly() { sorts_properly::<i16, _>(100, ska_sort) }

#[test]
fn medium_i16_sorts_properly() { sorts_properly::<i16, _>(20000, ska_sort) }

#[test]
fn large_i16_sorts_properly() { sorts_properly::<i16, _>(1000000, ska_sort) }


#[test]
fn small_i32_sorts_properly() { sorts_properly::<i32, _>(100, ska_sort) }

#[test]
fn medium_i32_sorts_properly() { sorts_properly::<i32, _>(20000, ska_sort) }

#[test]
fn large_i32_sorts_properly() { sorts_properly::<i32, _>(1000000, ska_sort) }


#[test]
fn small_i64_sorts_properly() { sorts_properly::<i64, _>(100, ska_sort) }

#[test]
fn medium_i64_sorts_properly() { sorts_properly::<i64, _>(20000, ska_sort) }

#[test]
fn large_i64_sorts_properly() { sorts_properly::<i64, _>(1000000, ska_sort) }

#[test]
fn small_isize_sorts_properly() { sorts_properly::<isize, _>(100, ska_sort) }

#[test]
fn medium_isize_sorts_properly() { sorts_properly::<isize, _>(20000, ska_sort) }

#[test]
fn large_isize_sorts_properly() { sorts_properly::<isize, _>(1000000, ska_sort) }

#[test]
fn small_f32_sorts_properly() { partial_sorts_properly::<f32, _>(
    100, |a| ska_sort_by_key(a, |f| OrderedFloat(*f)))
}

#[test]
fn medium_f32_sorts_properly() { partial_sorts_properly::<f32, _>(
    20000, |a| ska_sort_by_key(a, |f| OrderedFloat(*f)))
}

#[test]
fn large_f32_sorts_properly() { partial_sorts_properly::<f32, _>(
    1000000, |a| ska_sort_by_key(a, |f| OrderedFloat(*f)))
}

#[test]
fn small_f64_sorts_properly() { partial_sorts_properly::<f64, _>(
    100, |a| ska_sort_by_key(a, |f| OrderedFloat(*f)))
}

#[test]
fn medium_f64_sorts_properly() { partial_sorts_properly::<f64, _>(
    20000, |a| ska_sort_by_key(a, |f| OrderedFloat(*f)))
}

#[test]
fn large_f64_sorts_properly() { partial_sorts_properly::<f64, _>(
    1000000, |a| ska_sort_by_key(a, |f| OrderedFloat(*f)))
}

#[test]
fn small_f32_sorts_properly_direct_key() { partial_sorts_properly::<f32, _>(
    100, |a| ska_sort_by_key(a, |f| f.to_radix_key()))
}

#[test]
fn medium_f32_sorts_properly_direct_key() { partial_sorts_properly::<f32, _>(
    20000, |a| ska_sort_by_key(a, |f| f.to_radix_key()))
}

#[test]
fn large_f32_sorts_properly_direct_key() { partial_sorts_properly::<f32, _>(
    1000000, |a| ska_sort_by_key(a, |f| f.to_radix_key()))
}

#[test]
fn small_f64_sorts_properly_direct_key() { partial_sorts_properly::<f64, _>(
    100, |a| ska_sort_by_key(a, |f| f.to_radix_key()))
}

#[test]
fn medium_f64_sorts_properly_direct_key() { partial_sorts_properly::<f64, _>(
    20000, |a| ska_sort_by_key(a, |f| f.to_radix_key()))
}

#[test]
fn large_f64_sorts_properly_direct_key() { partial_sorts_properly::<f64, _>(
    1000000, |a| ska_sort_by_key(a, |f| f.to_radix_key()))
}
