mod common;
use common::{array_sorts_properly};
use rdx::ska_sort::{ska_sort};

#[test]
fn small_short_u8_sorts_properly() { array_sorts_properly::<u8, _>(100, 100, ska_sort) }

#[test]
fn small_medium_u8_sorts_properly() { array_sorts_properly::<u8, _>(100, 10000, ska_sort) }

#[test]
fn medium_short_u8_sorts_properly() { array_sorts_properly::<u8, _>(20000, 100, ska_sort) }

#[test]
fn large_short_u8_sorts_properly() { array_sorts_properly::<u8, _>(1000000, 100, ska_sort) }
