# rdx

[![](https://docs.rs/rdx/badge.svg)](https://docs.rs/rdx/)
[![](https://img.shields.io/crates/v/rdx.svg)](https://crates.io/crates/rdx)
[![](https://img.shields.io/crates/d/rdx.svg)](https://crates.io/crates/rdx)

`rdx` is a collection of generic algorithms and traits designed to make using radix sort easier
both for primitive and custom data types.

Radix sort has excellent performance characteristics, but has more requirements on the keys to be
sorted, and hence is somewhat rarely used. The goal of this package is to provide easy-to-use
radix sort implementations for a variety of types and to make it easy to implement radix sort
for your own custom types.

Eventually, we plan to add a hybrid radix-comparison sort so as to allow obtaining the benefits
of radix sorts for compound types in general, even if all the components only satisfy `Ord`.

License: MIT
