/*!
An implementation of [american flag sort](https://en.wikipedia.org/wiki/american_flag_sort)
*/

use super::*;
use util::*;

/// Sort an array using american flag sort with byte keys.
/// Pass in partition arrays, which are assumed have `count_or_offset` pre-initialized
/// Returns the number of partitions there are and the highest touched partition.
/// Always inlined so as to generate new code for each constant `maximum_byte`. Use to build
/// other algorithms.
#[inline(always)]
pub fn american_flag_byte_sort_on_partitions_always_inlined<T, E>(
    arr: &mut [T], extract_key: E,
    maximum_byte: u8,
    start_offset: usize,
    partitions: &mut [PartitionInfo]
) -> (u16, u8) where E: Fn(&T) -> u8, T: core::fmt::Debug {
    // Step 1: check partitions
    assert!(partitions.len() >= maximum_byte as usize);

    // Step 2: initialize offsets
    let mut total = start_offset;
    let mut num_partitions = 0;
    let mut highest_partition = 0;
    for i in 0..=maximum_byte as usize {
        let count = partitions[i].count_or_offset;
        if count != 0 { num_partitions += 1; highest_partition = i; }
        partitions[i].count_or_offset = total;
        total += count;
        partitions[i].next_offset = total;
    }

    // Step 3: swap into appropriate places
    for i in 0..=highest_partition {
        println!("Dealing with #{} = {:?}", i, partitions[i]);
        while partitions[i].len() > 0 {
            let start = partitions[i].count_or_offset;
            let key = extract_key(&arr[start]) as usize;
            if key == i {
                println!("arr[{}] = {:?} is in the right bucket", start, arr[start]);
                partitions[i].count_or_offset += 1
            }
            else {
                println!("arr[{}] is in the wrong bucket", start);
                while extract_key(&arr[partitions[key].count_or_offset]) as usize == key {
                    println!("arr[{}] is in the right bucket TWO", partitions[key].count_or_offset);
                    partitions[key].count_or_offset += 1;
                }
                println!("arr[{}] is in the wrong bucket TWO", partitions[key].count_or_offset);
                arr.swap(partitions[key].count_or_offset, partitions[i].count_or_offset);
                partitions[key].count_or_offset += 1;
            }
        }
    }

    println!("DONE\n\n");

    (num_partitions as u16, highest_partition as u8)
}

/// Sort an array using american flag sort with byte keys.
/// Pass in partition arrays, which are assumed have `count_or_offset` pre-initialized
/// Returns the number of partitions there are and the highest touched partition.
pub fn american_flag_byte_sort_on_partitions<T, E>(
    arr: &mut [T], extract_key: E,
    maximum_byte: u8,
    start_offset: usize,
    partitions: &mut [PartitionInfo]
) -> (u16, u8) where E: Fn(&T) -> u8, T: core::fmt::Debug {
    american_flag_byte_sort_on_partitions_always_inlined(
        arr, extract_key, maximum_byte, start_offset, partitions
    )
}

/// Sort an array using american flag sort with byte keys.
pub fn american_flag_byte_sort<T, E>(arr: &mut [T], extract_key: E) where E: Fn(&T) -> u8, T: core::fmt::Debug {
    // Otherwise
    let mut partitions = [PartitionInfo::default(); 256];
    let start = initialize_partitions(
        &arr, &extract_key, |_| 0, 255, |_| false, &mut partitions
    );
    assert_eq!(start, (0, 0));
    american_flag_byte_sort_on_partitions_always_inlined(
        arr,
        extract_key,
        255,
        0,
        &mut partitions
    );
}


#[cfg(test)]
mod tests {
    use std::prelude::v1::*;
    use std::cmp::{min, max};

    use super::*;
    use rand::distributions::{Distribution, Standard};

    /*
    fn string_sorter() -> impl RadixSortStrategy<&'static str> {
        RadixSortStrategyWith {
            bytes: KeyBytesWith {
                bytes: |x: &&str, b| { x.as_bytes()[b] },
                len: |x: &&str| x.len()
            },
            sort: TrySortWith(
                |arr: &mut [&str], _, _, force|
                    if force || arr.len() <= 2 { arr.sort(); true } else { false }
            )
        }
    }
    */

    /// Get a random vector of n elements from the standard distribution
    fn random_vec<T>(n: usize) -> Vec<T> where Standard: Distribution<T> {
        use rand::{Rng, SeedableRng};
        let mut list: Vec<T> = Vec::with_capacity(n);
        let mut rng = rand_xoshiro::Xoroshiro64StarStar::from_seed([0; 8]);
        for _ in 0..n { list.push(rng.gen()) }
        list
    }

    /// Test that calling `american_flag_byte_sort` on an empty list doesn't panic, even if the
    /// key extraction function always does
    #[test]
    fn empty_list_doesnt_panic() {
        american_flag_byte_sort(&mut [], |_: &u8| panic!("Bad!"))
    }

    /// Test that calling `american_flag_byte_sort` on a single element list does nothing
    #[test]
    fn single_element_list_doesnt_change() {
        for i in 0..256 {
            let mut single = [i as u8];
            american_flag_byte_sort(&mut single, |x| *x);
            assert_eq!(single[0], i as u8);
        }
    }

    /// Test that calling `american_flag_byte_sort` on two element lists sorts them
    #[test]
    fn two_element_lists_are_sorted_properly() {
        for i in 0..32 {
            for j in 0..32 {
                let mut pair = [i as u8, j as u8];
                american_flag_byte_sort(&mut pair, |x| *x);
                assert_eq!(pair, [min(i, j) as u8, max(i, j) as u8]);
            }
        }
    }

    /// Test that calling `american_flag_byte_sort` on three element lists sorts them
    #[test]
    fn three_element_lists_are_sorted_properly() {
        for i in 0..8 {
            for j in 0..2 {
                for k in 5..13 {
                    let mut list = [i, j, k];
                    let mut sorted = [i, j, k];
                    sorted.sort();
                    american_flag_byte_sort(&mut list, |x| *x);
                    assert_eq!(list, sorted);
                }
            }
        }
    }

    /// Test that calling `american_flag_byte_sort` on four element lists sorts them
    #[test]
    fn four_element_lists_are_sorted_properly() {
        for i in 0..2 {
            for j in 1..9 {
                for k in 0..2 {
                    for l in 0..2 {
                        let mut list = [i, j, k, l];
                        let mut sorted = [i, j, k, l];
                        sorted.sort();
                        american_flag_byte_sort(&mut list, |x| *x);
                        assert_eq!(list, sorted);
                    }
                }
            }
        }
    }

    /// Test that calling `american_flag_byte_sort` on five element lists sorts them
    #[test]
    fn five_element_lists_are_sorted_properly() {
        for x1 in 0..2 {
            for x2 in 10..11 {
                for x3 in 0..2 {
                    for x4 in 1..9 {
                        for x5 in 0..2 {
                            let mut list = [x1, x2, x3, x4, x5];
                            let mut sorted = list;
                            sorted.sort();
                            american_flag_byte_sort(&mut list, |x| *x);
                            assert_eq!(list, sorted);
                        }
                    }
                }
            }
        }
    }

    /// Test that `american_flag_byte_sort` properly sorts a long array of random bytes
    #[test]
    fn large_random_list_is_sorted_properly() {
        let size = 39482;
        let mut list = random_vec(size);
        let mut sorted = list.clone();
        sorted.sort();
        american_flag_byte_sort(&mut list, |x| *x);
        assert_eq!(list, sorted);
    }

    /// Test that `american_flag_byte_sort_on_partitions` properly sorts the end of an array of bytes
    #[test]
    fn end_of_list_sorted_properly() {
        let mut list = [7, 0, 3, 1, 11, 253, 2, 34, 2];
        let mut partitions = [PartitionInfo::default(); 256];
        let start = initialize_partitions(
            &list[3..], |x| *x, |_| 0, 255, |_| false, &mut partitions
        );
        assert_eq!(start, (0, 0));
        american_flag_byte_sort_on_partitions(&mut list, |x| *x, 255, 3, &mut partitions);
        let sorted = [1, 2, 2, 11, 34, 253];
        assert_eq!(list[3..], sorted);
    }

    /// Test that `american_flag_byte_sort_on_partitions` properly sorts the end of a large array of
    // bytes
    #[test]
    fn end_of_large_list_sorted_properly() {
        let size = 63543;
        let mut list = random_vec(size);
        let split = (2 * size) / 3;
        let mut sorted = Vec::from(&list[split..]);
        sorted.sort();
        let mut partitions = [PartitionInfo::default(); 256];
        let start = initialize_partitions(
            &list[split..], |x| *x, |_| 0, 255, |_| false, &mut partitions
        );
        assert_eq!(start, (0, 0));
        american_flag_byte_sort_on_partitions(&mut list, |x| *x, 255, split, &mut partitions);
        assert_eq!(sorted, &list[split..]);
    }

    /*
    /// Test that `ska_sort_by_strategy` properly sorts a small array of small 64-bit integers
    #[test]
    fn small_array_of_small_u64_is_sorted_properly() {
        let mut list: Vec<u64> = vec![0, 5, 87, 3, 6, 2];
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort(&mut list);
        assert_eq!(list, sorted);
    }

    /// Test that `ska_sort_by_strategy` properly sorts an array of bytes bigger than 256
    #[test]
    fn bigger_than_byte_array_of_random_bytes_is_sorted_properly() {
        let size = 257;
        let mut list: Vec<u8> = random_vec(size);
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort(&mut list);
        for (i, (l, s)) in list.iter().zip(sorted.iter()).enumerate() {
            if l != s {
                panic!("Difference: list[{}] = {} != sorted[{}] = {}", i, l, i, s)
            }
        }
    }

    /// Test that `ska_sort_by_strategy` properly sorts an array of >256 16 bit integers
    #[test]
    fn bigger_than_byte_array_of_random_u16_is_sorted_properly() {
        let size = 257;
        let mut list: Vec<u16> = random_vec(size);
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort(&mut list);
        for (i, (l, s)) in list.iter().zip(sorted.iter()).enumerate() {
            if l != s {
                panic!("Difference: list[{}] = {} != sorted[{}] = {}", i, l, i, s)
            }
        }
    }

    /// Test that `ska_sort_by_strategy` properly sorts an array of >256 32-bit integers
    #[test]
    fn bigger_than_byte_array_of_random_u32_is_sorted_properly() {
        let size = 259;
        let mut list: Vec<u32> = random_vec(size);
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort(&mut list);
        for (i, (l, s)) in list.iter().zip(sorted.iter()).enumerate() {
            if l != s {
                panic!("Difference: list[{}] = {} != sorted[{}] = {}", i, l, i, s)
            }
        }
    }

    /// Test that `ska_sort_by_strategy` properly sorts a small array of random 64 bit integers
    #[test]
    fn small_array_of_random_u64_is_sorted_properly() {
        let size = 255;
        let mut list: Vec<u64> = random_vec(size);
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort(&mut list);
        for (i, (l, s)) in list.iter().zip(sorted.iter()).enumerate() {
            if l != s {
                panic!("Difference: list[{}] = {} != sorted[{}] = {}", i, l, i, s)
            }
        }
    }

    /// Test that `ska_sort_by_strategy` properly sorts an array of >256 random 64 bit integers
    #[test]
    fn bigger_than_byte_array_of_random_u64_is_sorted_properly() {
        let size = 257;
        let mut list: Vec<u64> = random_vec(size);
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort(&mut list);
        for (i, (l, s)) in list.iter().zip(sorted.iter()).enumerate() {
            if l != s {
                panic!("Difference: list[{}] = {} != sorted[{}] = {}", i, l, i, s)
            }
        }
    }

    /// Test that `ska_sort_by_strategy` properly sorts a medium array of random 64 bit integers
    #[test]
    fn medium_array_of_random_u64_is_sorted_properly() {
        let size = 1515;
        let mut list: Vec<u64> = random_vec(size);
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort(&mut list);
        for (i, (l, s)) in list.iter().zip(sorted.iter()).enumerate() {
            if l != s {
                panic!("Difference: list[{}] = {} != sorted[{}] = {}", i, l, i, s)
            }
        }
    }

    /// Test that `ska_sort_by_strategy` properly sorts a large array of random bytes
    #[test]
    fn large_array_of_random_bytes_is_sorted_properly() {
        let size = 1000000;
        let mut list: Vec<u8> = random_vec(size);
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort(&mut list);
        for (i, (l, s)) in list.iter().zip(sorted.iter()).enumerate() {
            if l != s {
                panic!("Difference: list[{}] = {} != sorted[{}] = {}", i, l, i, s)
            }
        }
    }

    /// Test that `ska_sort_by_strategy` properly sorts a medium array of random 16-bit integers
    #[test]
    fn medium_array_of_random_u16s_is_sorted_properly() {
        let size = 2000;
        let mut list: Vec<u16> = random_vec(size);
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort(&mut list);
        for (i, (l, s)) in list.iter().zip(sorted.iter()).enumerate() {
            if l != s {
                panic!("Difference: list[{}] = {} != sorted[{}] = {}", i, l, i, s)
            }
        }
    }

    /// Test that `ska_sort_by_strategy` properly sorts a medium array of random 16-bit integers
    #[test]
    fn large_array_of_random_u16s_is_sorted_properly() {
        let size = 200000;
        let mut list: Vec<u16> = random_vec(size);
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort(&mut list);
        for (i, (l, s)) in list.iter().zip(sorted.iter()).enumerate() {
            if l != s {
                panic!("Difference: list[{}] = {} != sorted[{}] = {}", i, l, i, s)
            }
        }
    }

    /// Test that `ska_sort_by_strategy` properly sorts a large array of random 64 bit integers
    #[test]
    fn large_array_of_random_u64_is_sorted_properly() {
        let size = 200000;
        let mut list: Vec<u64> = random_vec(size);
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort(&mut list);
        for (i, (l, s)) in list.iter().zip(sorted.iter()).enumerate() {
            if l != s {
                panic!("Difference: list[{}] = {} != sorted[{}] = {}", i, l, i, s)
            }
        }
    }

    /// Test that `ska_sort_by_strategy` properly sorts a small array of random strings
    /// with no common prefix
    #[test]
    fn small_array_of_strings_with_no_common_prefix_is_sorted_properly() {
        let mut list: Vec<&str> = vec!["a", "fgh", "e", "g", "hello"];
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort_by_strategy(
            &mut list,
            RadixSortStrategyWith {
                bytes: KeyBytesWith {
                    bytes: |x: &&str, b| { x.as_bytes()[b] },
                    len: |x: &&str| x.len()
                },
                sort: TrySortWith(
                    |arr: &mut [&str], _, _, force|
                        if force || arr.len() <= 2 { arr.sort(); true } else { false }
                )
            }
        );
        assert_eq!(list, sorted);
    }

    /// Test that `ska_sort_by_strategy` properly sorts an array of 2-bit strings
    #[test]
    fn small_array_of_bit_strings_is_sorted_properly() {
        let mut list: Vec<&str> = [
            &["ba"; 2][..],
            &["ab"; 2][..],
            &["aa"; 1][..],
            &["bb"; 1][..]
        ].concat();
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort_by_strategy(&mut list, string_sorter());
        assert_eq!(list, sorted);
    }

    /// Test that `ska_sort_by_strategy` properly sorts a small array of strings
    #[test]
    fn small_array_of_strings_is_sorted_properly() {
        let mut list: Vec<&str> = vec![
            "hello",
            "goodbye",
            "hello world",
            "hello planet",
            "goodbye world",
            "goodbye planet"
            ];
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort_by_strategy(
            &mut list,
            RadixSortStrategyWith {
                bytes: KeyBytesWith {
                    bytes: |x: &&str, b| { x.as_bytes()[b] },
                    len: |x: &&str| x.len()
                },
                sort: TrySortWith(
                    |arr: &mut [&str], _, _, force|
                        if force || arr.len() <= 2 { arr.sort(); true } else { false }
                )
            }
        );
        assert_eq!(list, sorted);
    }

    /// Test that `ska_sort_by_strategy` properly sorts a tiny array of strings
    #[test]
    fn tiny_array_of_strings_is_sorted_properly() {
        let mut list: Vec<&str> = vec![
            "a1 -- bucket 0",
            "b2 -- bucket 1",
            "a2 -- bucket 0",
            "",
            "b1 -- bucket 1"
            ];
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort_by_strategy(&mut list, string_sorter());
        assert_eq!(list, sorted);
    }

    /// Test that `ska_sort_by_strategy` properly sorts a small array of strings of
    /// increasing length
    #[test]
    fn small_array_of_increasing_prefixes_is_sorted_properly() {
        let mut list: Vec<&str> = vec![
            "",
            "a",
            "ab",
            "abc",
            "abcde",
            "abcdef"
            ];
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort_by_strategy(&mut list, string_sorter());
        assert_eq!(list, sorted);
    }
    */

}
