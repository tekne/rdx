/*!
Utilities for radix-sorting floating point types
*/

use super::ConstRadixSortKey;
use core::hash::{Hash, Hasher};
use core::cmp::Ordering;

/// The trait implemented by floating point values which can be radix-ordered
pub trait Float: PartialEq + PartialOrd {
    /// The radix sort key which can be used to sort this value
    type KeyType: ConstRadixSortKey + Hash;
    /// Convert a value to its associated radix-sort key
    fn to_radix_key(&self) -> Self::KeyType;
}

/// A radix-ordered floating point
#[derive(Debug, Copy, Clone)]
pub struct OrderedFloat<F>(pub F);

impl<F: Float> OrderedFloat<F> {
    /// Get the radix-sort key for this ordered float
    pub fn key(&self) -> F::KeyType { self.0.to_radix_key() }
}

impl Float for f32 {
    type KeyType = u32;
    fn to_radix_key(&self) -> u32 {
        let sign_bit = 0x1 << 31;
        let as_int = u32::from_ne_bytes(self.to_ne_bytes());
        if sign_bit & as_int != 0 { !as_int } else { as_int ^ sign_bit }
    }
}
impl Float for f64 {
    type KeyType = u64;
    fn to_radix_key(&self) -> u64 {
        let sign_bit = 0x1 << 63;
        let as_int = u64::from_ne_bytes(self.to_ne_bytes());
        if sign_bit & as_int != 0 { !as_int } else { as_int ^ (0x1 << 63) }
    }
}

impl<F: Float> PartialEq for OrderedFloat<F> {
    fn eq(&self, other: &OrderedFloat<F>) -> bool { self.key().eq(&other.key()) }
}

impl<F: Float> Eq for OrderedFloat<F> {}

impl<F: Float> PartialOrd for OrderedFloat<F> {
    fn partial_cmp(&self, other: &OrderedFloat<F>) -> Option<Ordering> {
        self.key().partial_cmp(&other.key())
    }
}

impl<F: Float> Ord for OrderedFloat<F> {
    fn cmp(&self, other: &OrderedFloat<F>) -> Ordering { self.key().cmp(&other.key()) }
}

impl<F: Float> Hash for OrderedFloat<F> {
    fn hash<H: Hasher>(&self, hasher: &mut H) { self.key().hash(hasher) }
}

impl<F: Float> ConstRadixSortKey for OrderedFloat<F> {
    const CONST_KEY_LEN: usize = F::KeyType::CONST_KEY_LEN;
    #[inline(always)]
    fn const_key_byte(&self, byte: usize) -> u8 { self.0.to_radix_key().const_key_byte(byte) }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand::distributions::{Distribution, Standard};
    use rand::{Rng, SeedableRng};
    use std::prelude::v1::*;

    /// Get a random vector of `n` elements from the standard distribution
    fn random_vec<T, R>(n: usize, rng: &mut R) -> Vec<T>
    where Standard: Distribution<T>, R: Rng {
        let mut list: Vec<T> = Vec::with_capacity(n);
        for _ in 0..n { list.push(rng.gen()) }
        list
    }

    #[test]
    fn ordered_f32s_are_sorted_properly() {
        let mut rng = rand_xoshiro::Xoroshiro64StarStar::from_seed([5; 8]);
        let mut floats: Vec<f32> = random_vec(1000000, &mut rng);
        let mut sorted_floats = floats.clone();
        sorted_floats.sort_unstable_by(|a, b| a.partial_cmp(b).unwrap());
        floats.sort_unstable_by_key(|f| OrderedFloat(*f));
        assert_eq!(sorted_floats, floats);
    }

    #[test]
    fn ordered_f64s_are_sorted_properly() {
        let mut rng = rand_xoshiro::Xoroshiro64StarStar::from_seed([5; 8]);
        let mut floats: Vec<f64> = random_vec(1000000, &mut rng);
        let mut sorted_floats = floats.clone();
        sorted_floats.sort_unstable_by(|a, b| a.partial_cmp(b).unwrap());
        floats.sort_unstable_by_key(|f| OrderedFloat(*f));
        assert_eq!(sorted_floats, floats);
    }

}
