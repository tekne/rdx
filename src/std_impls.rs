/*!
`RadixSortKey` implementations for standard-library types. Only included if the `std` feature is
enabled.
*/
use super::*;

impl<K> RadixSortKey for std::prelude::v1::Vec<K> where K: ConstRadixSortKey, std::prelude::v1::Vec<K>: Ord {
    #[inline(always)] fn key_len(&self) -> usize { (&self[..]).key_len() }
    #[inline(always)] fn key_byte(&self, byte: usize) -> u8 { (&self[..]).key_byte(byte) }
}
