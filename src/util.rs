/*!
Various utilities for building radix sorts
*/
use core::fmt::{self, Debug, Formatter};

/// A slice-stack. To avoid a dependency on `arrayvec`
pub struct SliceStack<'a, T> {
    /// The slice backing this stack
    pub backing: &'a mut [T],
    /// The size of the stack
    pub size: usize
}

impl<'a, T> SliceStack<'a, T> {
    /// Create a new slice stack
    #[inline(always)]
    pub fn new(backing: &'a mut [T]) -> SliceStack<'a, T> { SliceStack { backing, size: 0 } }
    /// Get the top of a slice stack
    #[inline]
    pub fn top(&self) -> Option<&T> {
        if self.size == 0 { None } else { Some(&self.backing[self.size - 1]) }
    }
    /// Mutably get the top of a slice stack
    #[inline]
    pub fn top_mut(&mut self) -> Option<&mut T> {
        if self.size == 0 { None } else { Some(&mut self.backing[self.size - 1]) }
    }
    /// Pop the last element from a slice stack
    #[inline]
    pub fn pop(&mut self) -> Option<&mut T> {
        if self.size == 0 { None } else { self.size -= 1; Some(&mut self.backing[self.size]) }
    }
    /// Push an element onto a slice stack. Return the element back if out of space, otherwise,
    /// return the old value at that position.
    #[inline]
    pub fn push(&mut self, mut elem: T) -> Result<T, T> {
        if self.size < self.backing.len() {
            core::mem::swap(&mut elem, &mut self.backing[self.size]);
            self.size += 1;
            Ok(elem)
        } else {
            Err(elem)
        }
    }
    /// Get this stack as a slice (a subslice of the backing)
    #[inline(always)] pub fn as_slice(&self) -> &[T] { &self.backing[..self.size] }
    /// Get this stack as a slice (a subslice of the backing)
    #[inline(always)] pub fn as_slice_mut(&mut self) -> &mut [T] { &mut self.backing[..self.size] }
    /// Get the capacity of this stack
    #[inline(always)] pub fn capacity(&self) -> usize { self.backing.len() }
    /// Check if this stack is full
    #[inline(always)] pub fn full(&self) -> bool { self.backing.len() == self.size }
}

impl<'a, T> Debug for SliceStack<'a, T> where [T]: Debug {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        (&self.backing[..self.size]).fmt(fmt)
    }
}

/// Partition information for an array
#[derive(Debug, Copy, Clone, Default)]
pub struct PartitionInfo {
    /// The beginning of a partition *or* the number of elements which should be in a partition
    /// depending on the phase of the algorithm.
    pub count_or_offset: usize,
    /// The end of the partition
    pub next_offset: usize
}

impl PartitionInfo {
    /// Create a partition for a given slice at a given offset
    #[inline(always)] pub fn slice_offset<T>(slice: &[T], offset: usize) -> PartitionInfo {
        PartitionInfo { count_or_offset: offset, next_offset: offset + slice.len() }
    }
    /// Get a slice of a partition from an array
    #[inline(always)] pub fn slice<'a, T>(&self, slice: &'a [T]) -> &'a [T] {
        &slice[self.count_or_offset..self.next_offset]
    }
    /// Get a mutable slice of a partition from an array
    #[inline(always)] pub fn slice_mut<'a, T>(&self, slice: &'a mut [T]) -> &'a mut [T] {
        &mut slice[self.count_or_offset..self.next_offset]
    }
    /// Get a slice up to the end of a partition from an array
    #[inline(always)] pub fn end<'a, T>(&self, slice: &'a [T]) -> &'a [T] {
        &slice[0..self.next_offset]
    }
    /// Get a mutable slice up to the end of a partition from an array
    #[inline(always)] pub fn end_mut<'a, T>(&self, slice: &'a mut [T]) -> &'a mut [T] {
        &mut slice[0..self.next_offset]
    }
    /// Filter this partition, removing all elements satisfying predicate `p` and moving them
    /// to the old beginning of the partition. Return the number of elements removed.
    #[inline(always)] pub fn filter<T, P>(&mut self, slice: &mut [T], p: P) -> usize
    where P: Fn(&T) -> bool {
        let new_start = partition(self.slice_mut(slice), p);
        self.count_or_offset += new_start;
        new_start
    }
    /// Filter this partition, removing all elements satisfying predicate `p` and moving them
    /// to the old beginning of the partition. Return the number of elements removed.
    #[inline(always)] pub fn filter_mut<T, P>(&mut self, slice: &mut [T], p: P) -> usize
    where P: FnMut(&T) -> bool {
        let new_start = partition_mut(self.slice_mut(slice), p);
        self.count_or_offset += new_start;
        new_start
    }
    /// Set this partition's beginning to the other's end
    #[inline(always)] pub fn stack(&mut self, other: PartitionInfo) {
        self.count_or_offset = other.next_offset
    }
    /// Get the length of this partition
    #[inline(always)] pub fn len(&self) -> usize { self.next_offset - self.count_or_offset }
    /// Check if this partition is valid
    #[inline(always)] pub fn is_valid<T>(&self, arr: &[T]) -> bool {
        self.next_offset >= self.count_or_offset && self.next_offset <= arr.len()
    }
}


/// Partition an array such that all elements for which `p` returns `true` are placed before all
/// elements for which `p` returns false. The order other than this is undefined.
/// Return the index of the first element for which `p` returns `false`, or the length of the
/// array if `p` is true for all elements.
#[inline] pub fn partition<T, P>(a: &mut [T], p: P) -> usize where P: Fn(&T) -> bool {
    partition_mut(a, p)
}

/// Partition an array such that all elements for which `p` returns `true` are placed before all
/// elements for which `p` returns false. The order other than this is undefined.
/// Return the index of the first element for which `p` returns `false`, or the length of the
/// array if `p` is true for all elements.
#[inline] pub fn partition_mut<T, P>(a: &mut [T], mut p: P) -> usize where P: FnMut(&T) -> bool {
    let mut true_end = 0;
    let l = a.len();
    for i in 0..l {
        if p(&a[i]) {
            if i != true_end { a.swap(i, true_end); }
            true_end += 1;
        }
    }
    true_end
}

/// Initialize a partition array for use by ska sort up to the `maximum_byte`-th entry.
/// Return the partition offset, if any.
/// Skip over short keys, return how many were skipped over.
pub fn initialize_partitions<T, E, P, S>(
    arr: &[T],
    extract_key: E,
    extract_partition: P,
    maximum_byte: u8,
    ignore_key: S,
    partitions: &mut [PartitionInfo]
) -> (usize, usize) where E: Fn(&T) -> u8, P: Fn(&T) -> u8, S: Fn(&T) -> bool {
    assert!(partitions.len() >= maximum_byte as usize);
    let mut ignored = 0;
    // Initialize array to hold counts
    for i in 0..=maximum_byte {
        partitions[i as usize].count_or_offset = 0;
    }
    let mut partition = None;
    // Go backwards over the array to find the start of the partition
    for (i, obj) in arr.iter().enumerate().rev() {

        if ignore_key(obj) { ignored += 1; continue }

        let obj_partition = extract_partition(obj);

        if let Some(partition) = partition {
            if obj_partition != partition { return (i + 1, ignored) }
        } else { partition = Some(obj_partition) }

        partitions[extract_key(obj).min(maximum_byte) as usize].count_or_offset += 1;

    }
    (0, ignored)
}


/// Unroll a loop four times
#[inline(always)] pub(crate) fn unroll_four_times<T, F>(
    mut obj: &mut[T],
    begin_offset: usize, iterations: usize,
    mut to_call: F)
    where F: FnMut(&mut[T], usize) {
    let count = iterations / 4;
    let mut begin = begin_offset;
    let remainder = iterations - count * 4;
    for _ in 0..count {
        to_call(&mut obj, begin);
        begin += 1;
        to_call(&mut obj, begin);
        begin += 1;
        to_call(&mut obj, begin);
        begin += 1;
        to_call(&mut obj, begin);
        begin += 1;
    }
    if remainder >= 3 { to_call(&mut obj, begin); begin += 1 }
    if remainder >= 2 { to_call(&mut obj, begin); begin += 1 }
    if remainder >= 1 { to_call(&mut obj, begin); }
}

#[cfg(test)]
mod tests {
    use super::*;

    /// Test that `partition` satisfies its invariants on a list of booleans
    #[test]
    fn partition_works_on_array_of_bools() {
        let mut bools = [false, true, false, false, true, true, false, false, true];
        assert_eq!(partition(&mut bools, |b| *b), 4);
        assert_eq!(bools, [true, true, true, true, false, false, false, false, false]);
    }

    /// Test stacking `PartitionInfo` structs works
    #[test]
    fn partition_info_stacking_works() {
        let first = PartitionInfo { count_or_offset: 0, next_offset: 37 };
        let second = PartitionInfo { count_or_offset: 134, next_offset: 53 };
        let mut stacked = second;
        stacked.stack(first);
        assert_eq!(stacked.count_or_offset, first.next_offset);
        assert_eq!(stacked.next_offset, second.next_offset);
    }

    /// Test that `initialize_partition` satisfies its invariants on a list of u16s
    #[test]
    fn initialize_partition_works_on_small_u16_array() {
        let u16s = [257, 258, 259, 0, 3, 23, 254, 3, 73];
        let mut partitions = [PartitionInfo::default(); 256];
        let start = initialize_partitions(
            &u16s,
            |x| (x % 256) as u8,
            |x| (x / 256) as u8,
            255,
            |_| false,
            &mut partitions
        );
        assert_eq!(start, (3, 0));

        let mut desired_partitions = [PartitionInfo::default(); 256];
        desired_partitions[0].count_or_offset = 1;
        desired_partitions[3].count_or_offset = 2;
        desired_partitions[23].count_or_offset = 1;
        desired_partitions[73].count_or_offset = 1;
        desired_partitions[254].count_or_offset = 1;

        for i in 0..256 {
            assert_eq!(partitions[i].count_or_offset, desired_partitions[i].count_or_offset)
        }
    }

}
