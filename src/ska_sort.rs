/*!
An implementation of [ska sort](https://probablydance.com/2016/12/27/i-wrote-a-faster-sorting-algorithm/),
based off the code found in the [blog post](https://probablydance.com/2016/12/27/i-wrote-a-faster-sorting-algorithm/)
and on [GitHub](https://github.com/skarupke/ska_sort/blob/master/ska_sort.hpp)
*/

use super::*;
use util::*;

/// Sort an array using ska sort with byte keys.
/// Pass in partition arrays, which are assumed have `count_or_offset` zeroed.
/// Returns the number of partitions there are and the highest touched partition.
/// Always inlined so as to generate new code for each constant `maximum_byte`. Use to build
/// other algorithms.
#[inline(always)]
pub fn ska_byte_sort_on_partitions_always_inlined<T, E>(
    arr: &mut [T], extract_key: E,
    maximum_byte: u8,
    start_offset: usize,
    partitions: &mut [PartitionInfo]
) -> (u16, u8) where E: Fn(&T) -> u8 {

    // Special cases
    if arr[start_offset..].len() == 0 { return (0, 0) }
    if arr[start_offset..].len() == 1 { return (1, extract_key(&arr[start_offset])) }
    if arr[start_offset..].len() == 2 {
        let k1 = extract_key(&arr[start_offset + 1]);
        let k2 = extract_key(&arr[start_offset]);
        if k1 < k2 {
            partitions[k2 as usize].count_or_offset = start_offset;
            partitions[k2 as usize].next_offset = start_offset + 1;
            partitions[k1 as usize].count_or_offset = start_offset + 1;
            partitions[k1 as usize].next_offset = start_offset + 2;
            arr.swap(0, 1);
            return (2, k1);
        }
        else if k1 > k2 {
            partitions[k1 as usize].count_or_offset = start_offset;
            partitions[k1 as usize].next_offset = start_offset + 1;
            partitions[k2 as usize].count_or_offset = start_offset + 1;
            partitions[k2 as usize].next_offset = start_offset + 2;
            return (2, k2);
        }
        else {
            partitions[k1 as usize].count_or_offset = start_offset + 0;
            partitions[k1 as usize].next_offset = start_offset + 2;
            return (1, k1)
        }
    }

    debug_assert!(start_offset < arr.len());

    assert!(partitions.len() >= maximum_byte as usize + 1);

    let mut remaining_partitions = [0u8; 256];

    // Do a partition check:
    #[cfg(debug_assertions)]
    {
        let mut counts: [usize; 256] = [0; 256];
        let mut fail = false;
        for obj in arr[start_offset..].iter() {
            counts[extract_key(obj) as usize] += 1;
        }
        for i in 0..256 {
            if partitions[i].count_or_offset != counts[i] {
                println!(
                    "partitions[{}].count_or_offset = {} != counts[{}] = {}",
                    i, partitions[i].count_or_offset, i, counts[i]
                );
                fail = true;
            }
        }
        assert!(!fail);
    }

    let mut total = start_offset;
    let mut num_partitions = 0;
    let mut highest_partition = 0;
    for i in 0..=maximum_byte as usize {
        let count = partitions[i].count_or_offset;
        if count != 0 {
            partitions[i].count_or_offset = total;
            total += count;
            remaining_partitions[num_partitions] = i as u8;
            num_partitions += 1;
            highest_partition = i;
        }
        partitions[i].next_offset = total
    }

    let num_partitions = num_partitions; // Stop mutability
    debug_assert_ne!(arr[start_offset..].len(), 0);
    debug_assert_ne!(num_partitions, 0);
    debug_assert!(num_partitions <= 256);

    let mut last_remaining = num_partitions;
    while last_remaining > 1 {
        last_remaining = partition_mut(
            &mut remaining_partitions[0..last_remaining],
            |partition| {
                let partition = *partition as usize;
                let begin_offset = partitions[partition].count_or_offset;
                let end_offset = partitions[partition].next_offset;
                if begin_offset == end_offset { return false }

                unroll_four_times(
                    arr,
                    begin_offset,
                    end_offset - begin_offset,
                    |arr: &mut [T], ix: usize| {
                        #[cfg(debug_assertions)] {
                            if arr.len() <= ix {
                                panic!("Error: arr.len() = {} <= ix = {}", arr.len(), ix)
                            }
                            if ix < begin_offset {
                                panic!("Error: ix = {} < begin_offset = {}", ix, begin_offset)
                            }
                            if ix >= end_offset {
                                panic!("Error: ix = {} >= end_offset = {}", ix, end_offset)
                            }
                        }
                        let this_partition = extract_key(&arr[ix]) as usize;
                        let offset = partitions[this_partition].count_or_offset;
                        partitions[this_partition].count_or_offset += 1;
                        arr.swap(ix, offset);
                    });

                return begin_offset != end_offset;
            }
        )
    }

    (num_partitions as u16, highest_partition as u8)
}

/// Sort an array using ska sort with byte keys.
/// Pass in partition arrays, which are assumed have `count_or_offset` zeroed.
/// Returns the number of partitions there are and the highest touched partition.
pub fn ska_byte_sort_on_partitions<T, E>(
    arr: &mut [T], extract_key: E,
    maximum_byte: u8,
    start_offset: usize,
    partitions: &mut [PartitionInfo]
) -> (u16, u8) where E: Fn(&T) -> u8 {
    ska_byte_sort_on_partitions_always_inlined(
        arr, extract_key, maximum_byte, start_offset, partitions
    )
}

/// Sort an array using ska sort with byte keys.
pub fn ska_byte_sort<T, E>(arr: &mut [T], extract_key: E) where E: Fn(&T) -> u8 {
    // Otherwise
    let mut partitions = [PartitionInfo::default(); 256];
    let start = initialize_partitions(
        &arr, &extract_key, |_| 0, 255, |_| false, &mut partitions
    );
    assert_eq!(start, (0, 0));
    ska_byte_sort_on_partitions_always_inlined(
        arr,
        extract_key,
        255,
        0,
        &mut partitions
    );
}

#[derive(Debug, Copy, Clone, Default)]
struct DepthInfo {
    starting_index: usize,
    above: isize
}

impl SliceStack<'_, DepthInfo> {
    /// Get the current depth
    #[inline(always)] fn depth(&self) -> isize {
        self.top().map(|top| top.above).unwrap_or(0)
    }
    /// Get the current start
    #[inline(always)] fn start(&self) -> usize {
        self.top().map(|top| top.starting_index).unwrap_or(0)
    }
}

/// Get the next partition of an array to sort using multi-byte keys.
#[inline(always)] fn next_partition<T, S>(
    arr: &mut &mut [T],
    strategy: &mut S,
    iterations: usize,
    depth_stack: &mut SliceStack<DepthInfo>,
    partitions: &mut SliceStack<PartitionInfo>) -> Option<PartitionInfo>
    where S: RadixSortStrategy<T> {

    // Get this run's key length parameters
    let const_key_len = strategy.has_const_key_len();
    let max_key_len = if let Some(max_key_len) = strategy.max_key_len() { max_key_len as isize }
        else if const_key_len {
            if arr.len() == 0 { return None }
            strategy.key_len(&arr[0]) as isize // TODO: avoid recomputing over and over again
        } else {
            core::isize::MAX
        };

    let depth = depth_stack.depth();
    if depth >= max_key_len { return None } // We've exceeded max length!

    let current_start = depth_stack.start();

    // First: try to get a usable partition from the partition stack.
    let mut usable_partition = None;
    let mut multiple_usable = false;

    while let Some(mut partition) = partitions.pop().map(|p| *p) {
        // Fix partition
        if let Some(top) = partitions.top() { partition.stack(*top) }
        else { partition.count_or_offset = current_start }

        // Check the partition is valid
        debug_assert!(partition.is_valid(arr));

        // Filter partition
        let starting_index = partition.count_or_offset;
        if !const_key_len {
            partition.filter(
                arr,
                |key| strategy.key_len(key) <= (depth + 1) as usize
            );
            // Do a partition check:
            #[cfg(debug_assertions)]
            {
                for (i, obj) in partition.slice(arr).iter().enumerate() {
                    let key_len = strategy.key_len(obj);
                    if depth + 1 >= key_len as isize {
                        panic!(
                            "depth + 1 = {} >= key_len = {} in used for filtered partition {:?} @ {}",
                            depth + 1, key_len, partition, i
                        )
                    }
                }
            }
        }

        // Skip sorted/standard sortable partitions. Only attempt to sort at the end of the array.
        if partition.len() <= 1 || (usable_partition.is_none() && strategy.try_sort(
            partition.slice_mut(arr),
            depth, iterations, depth_stack.full()
        )) { // This partition is empty or has been sorted.
            // If we're at the end of the array, remvoe it from the array
                if usable_partition.is_none() {
                let mut tmp = &mut [][..];
                core::mem::swap(arr, &mut tmp);
                *arr = &mut tmp[..partition.count_or_offset];
            }
            continue
        }

        // Set the usable partition
        if usable_partition.is_none() {
            usable_partition = Some((starting_index, partition));
            continue
        }

        // If there's another partition, stop the search and push to the depth stack
        multiple_usable = true;
        break;
    }

    // If found, send the partition after initializing counts
    if let Some((starting_index, partition)) = usable_partition {

        // Filter all remaining partitions
        while let Some(mut partition) = partitions.pop().map(|p| *p) {
            // Fix partition
            if let Some(top) = partitions.top() { partition.stack(*top) }
            else { partition.count_or_offset = current_start }

            if !const_key_len {
                partition.filter(
                    arr,
                    |key| strategy.key_len(key) <= (depth + 1) as usize
                );
                // Do a partition check:
                #[cfg(debug_assertions)]
                {
                    for (i, obj) in partition.slice(arr).iter().enumerate() {
                        let key_len = strategy.key_len(obj);
                        if depth >= key_len as isize {
                            panic!(
                                "depth + 1 = {} >= key_len = {} in used for filtered partition {:?} @ {}",
                                depth + 1, key_len, partition, i
                            )
                        }
                    }
                }
            }
        }

        // Handle depth stack manipulations as appropriate
        if multiple_usable {
            depth_stack.push(DepthInfo { above: depth + 1, starting_index })
                .expect("Fatal error: subsort should never fail while depth stack is full!");
        } else if let Some(top) = depth_stack.top_mut() {
            top.above += 1
        } else {
            panic!("Fatal error: depth stack should never be empty")
        }

        let depth = depth_stack.depth();

        let ixs = initialize_partitions(
            partition.slice(arr),
            |key| strategy.key_byte(key, depth as usize),
            |_| 0,
            255,
            |_| false,
            &mut partitions.backing);
        debug_assert_eq!(ixs, (0, 0));

        // Do a partition check:
        #[cfg(debug_assertions)]
        {
            let mut counts: [usize; 256] = [0; 256];
            for obj in partition.slice(arr) {
                counts[strategy.key_byte(obj, depth as usize) as usize] += 1;
            }
            for i in 0..256 {
                debug_assert_eq!(partitions.backing[i].count_or_offset, counts[i]);
            }
        }

        return Some(partition)
    }

    let depth = depth_stack.depth();
    if depth == 0 { return None } // We're done
    let mut starting_index = depth_stack.start();
    if depth_stack.pop().is_none() { return None }


    // Otherwise: (try to) initialize a new set of partitions
    loop {

        // Edge case: small array
        if arr.len() <= 1 {
            return None
        }

        // Cut off the end of the array
        {
            let mut tmp = &mut [][..];
            core::mem::swap(arr, &mut tmp);
            *arr = &mut tmp[..starting_index];
        }

        // Edge case: small array
        if arr.len() <= 1 {
            return None
        }

        // Otherwise, initialize partitions
        let (start, ignored) = initialize_partitions(
            arr,
            |key| strategy.key_byte(key, depth as usize),
            |key| strategy.key_byte(key, depth as usize - 1),
            255,
            |key| strategy.key_len(key) <= depth as usize,
            &mut partitions.backing);

        // Get the result partition
        let partition = PartitionInfo {
            count_or_offset: start + ignored,
            next_offset: starting_index
        };

        debug_assert!(partition.is_valid(arr));
        #[cfg(debug_assertions)] {
            let mut indices = 0;
            let mut min_index = std::usize::MAX;
            let mut max_index = 0;
            let mut min_len = std::usize::MAX;
            let mut max_len = 0;
            for (i, obj) in arr[start..start + ignored].iter().enumerate() {
                let key_len = strategy.key_len(obj);
                if depth < key_len as isize {
                    indices += 1;
                    min_index = min_index.min(i);
                    max_index = max_index.max(i);
                    min_len = min_len.min(key_len);
                    max_len = max_len.max(key_len);
                }
                if indices != 0 {
                    panic!(
                        "depth = {} < key_len in {}..={} in scavenged partition {:?} (start = {}, ignored = {}) @ indices {}..={}",
                        depth, min_len, max_len, partition, start, ignored, min_index, max_index
                    )
                }
            }
            let mut indices = 0;
            let mut min_index = std::usize::MAX;
            let mut max_index = 0;
            let mut min_len = std::usize::MAX;
            let mut max_len = 0;
            for (i, obj) in partition.slice(arr).iter().enumerate() {
                let key_len = strategy.key_len(obj);
                if depth >= key_len as isize {
                    indices += 1;
                    min_index = min_index.min(i);
                    max_index = max_index.max(i);
                    min_len = min_len.min(key_len);
                    max_len = max_len.max(key_len);
                }
            }
            if indices != 0 {
                panic!(
                    "depth = {} >= key_len in {}..={} in scavenged partition {:?} (start = {}, ignored = {}) @ indices {}..={}",
                    depth, min_len, max_len, partition, start, ignored, min_index, max_index
                )
            }
        }

        if partition.len() <= 1 || (usable_partition.is_none() && strategy.try_sort(
            partition.slice_mut(arr),
            depth, iterations, depth_stack.full()
        )) {
            // This partition is empty or has been sorted.
            // Cut it from the array
            let mut tmp = &mut [][..];
            core::mem::swap(arr, &mut tmp);
            *arr = &mut tmp[..start];
            starting_index = start;
            continue
        } else {
            depth_stack.push(DepthInfo { above: depth as isize, starting_index: start })
                .expect("Fatal error: depth stack should never be full here!");
            debug_assert!(partition.is_valid(arr));
            return Some(partition)
        }
    }
}

/// Sort an array with ska sort using multi-byte keys, with a given strategy
pub fn ska_sort_by_strategy<T, S>(mut arr: &mut [T], mut strategy: S)
where S: RadixSortStrategy<T> {

    // Trivial edge case
    if arr.len() == 0 { return }

    let mut depth_backing = [DepthInfo::default(); DEPTH_STACK_SIZE];
    let mut depth_stack = SliceStack::new(&mut depth_backing);
    let mut partitions_backing = [PartitionInfo::default(); 256];
    let mut partitions = SliceStack::new(&mut partitions_backing);
    let mut iterations = 0;

    // Optimizations
    if strategy.has_const_key_len() {
        let key_len = strategy.key_len(&arr[0]);
        match key_len {
            0 => return, // No need to sort
            1 => { //  Delegate to byte sort
                ska_byte_sort(arr, |obj| strategy.key_byte(obj, 0));
                return
            },
            _ => {}
        }
    }

    // Step 1: prepare the partition stack
    let arr_partition = PartitionInfo::slice_offset(arr, 0);
    partitions.push(arr_partition)
        .expect("Fatal error: partition stack must have a capacity of at least one");
    depth_stack.push(DepthInfo { above: -1, starting_index: 0 })
        .expect("Fatal error: partition stack must have a capacity of at least one");

    // Step 2: Sort!
    while let Some(partition) = next_partition(
        &mut arr,
        &mut strategy,
        iterations, &mut depth_stack, &mut partitions
    ) {
        // Check partition validity (debug)
        debug_assert!(partition.is_valid(arr));
        // Increment iteration counter
        iterations += 1;
        // Get depth
        let depth = depth_stack.depth();
        // Check depth
        #[cfg(debug_assertions)] {
            for (i, elem) in partition.slice(arr).iter().enumerate() {
                let key_len = strategy.key_len(elem) as isize;
                if key_len <= depth {
                    panic!(
                        "key_len = {} <= depth = {} @ {:?}[{}] (iterations = {})",
                        key_len, depth, partition, i, iterations
                    );
                }
            }
        }
        // Radix sort the partition
        let (num_parts, max_part) = ska_byte_sort_on_partitions_always_inlined(
            partition.end_mut(arr),
            |key| strategy.key_byte(key, depth as usize),
            255,
            partition.count_or_offset,
            &mut partitions.backing
        );
        // Check partition has some parts
        debug_assert_ne!(num_parts, 0);
        partitions.size = max_part as usize + 1;
    }
}

/// Delegate small sorts to sorting functions more optimized for them, returning true.
/// Otherwise, return false
#[inline(always)]
pub fn delegate_sort<T, E, K>(
    extract_key: E, arr: &mut [T], depth: isize, _iter: usize, force: bool
) -> bool where E: Fn(&T) -> K, K: RadixSortKey {
    if force || arr.len() <= K::std_sort_size_threshold() || depth > K::depth_threshold() as isize
    { arr.sort_unstable_by_key(extract_key); true }
    else { false }
}

/// Ska sort an array, using a given key extraction function
#[inline(always)]
pub fn ska_sort_by_key<T, E, K>(arr: &mut [T], extract_key: E)
where E: Fn(&T) -> K + Copy, K: RadixSortKey {
    ska_sort_by_strategy(arr, key_strategy(extract_key))
}

/// Ska sort an array
#[inline(always)]
pub fn ska_sort<T>(arr: &mut [T]) where T: RadixSortKey {
    ska_sort_by_strategy(arr, DefaultStrategy)
}


#[cfg(test)]
mod tests {
    use std::prelude::v1::*;
    use super::*;
    use std::cmp::{max, min};
    use rand::distributions::{Distribution, Standard};

    /// Get a random vector of n elements from the standard distribution
    fn random_vec<T>(n: usize) -> Vec<T> where Standard: Distribution<T> {
        use rand::{Rng, SeedableRng};
        let mut list: Vec<T> = Vec::with_capacity(n);
        let mut rng = rand_xoshiro::Xoroshiro64StarStar::from_seed([0; 8]);
        for _ in 0..n { list.push(rng.gen()) }
        list
    }

    /// Test that calling `ska_byte_sort` on an empty list doesn't panic, even if the
    /// key extraction function always does
    #[test]
    fn empty_list_doesnt_panic() {
        ska_byte_sort(&mut [], |_: &u8| panic!("Bad!"))
    }

    /// Test that calling `ska_byte_sort` on a single element list does nothing
    #[test]
    fn single_element_list_doesnt_change() {
        for i in 0..256 {
            let mut single = [i as u8];
            ska_byte_sort(&mut single, |x| *x);
            assert_eq!(single[0], i as u8);
        }
    }

    /// Test that calling `ska_byte_sort` on two element lists sorts them
    #[test]
    fn two_element_lists_are_sorted_properly() {
        for i in 0..32 {
            for j in 0..32 {
                let mut pair = [i as u8, j as u8];
                ska_byte_sort(&mut pair, |x| *x);
                assert_eq!(pair, [min(i, j) as u8, max(i, j) as u8]);
            }
        }
    }

    /// Test that calling `ska_byte_sort` on three element lists sorts them
    #[test]
    fn three_element_lists_are_sorted_properly() {
        for i in 0..8 {
            for j in 0..2 {
                for k in 5..13 {
                    let mut list = [i, j, k];
                    let mut sorted = [i, j, k];
                    sorted.sort();
                    ska_byte_sort(&mut list, |x| *x);
                    assert_eq!(list, sorted);
                }
            }
        }
    }

    /// Test that calling `ska_byte_sort` on four element lists sorts them
    #[test]
    fn four_element_lists_are_sorted_properly() {
        for i in 0..2 {
            for j in 1..9 {
                for k in 0..2 {
                    for l in 0..2 {
                        let mut list = [i, j, k, l];
                        let mut sorted = [i, j, k, l];
                        sorted.sort();
                        ska_byte_sort(&mut list, |x| *x);
                        assert_eq!(list, sorted);
                    }
                }
            }
        }
    }

    /// Test that calling `ska_byte_sort` on five element lists sorts them
    #[test]
    fn five_element_lists_are_sorted_properly() {
        for x1 in 0..2 {
            for x2 in 10..11 {
                for x3 in 0..2 {
                    for x4 in 1..9 {
                        for x5 in 0..2 {
                            let mut list = [x1, x2, x3, x4, x5];
                            let mut sorted = list;
                            sorted.sort();
                            ska_byte_sort(&mut list, |x| *x);
                            assert_eq!(list, sorted);
                        }
                    }
                }
            }
        }
    }

    /// Test that `ska_byte_sort` properly sorts a long array of random bytes
    #[test]
    fn large_random_list_is_sorted_properly() {
        let size = 39482;
        let mut list = random_vec(size);
        let mut sorted = list.clone();
        sorted.sort();
        ska_byte_sort(&mut list, |x| *x);
        assert_eq!(list, sorted);
    }

    /// Test that `skay_byte_sort_on_partitions` properly sorts the end of an array of bytes
    #[test]
    fn end_of_list_sorted_properly() {
        let mut list = [7, 0, 3, 1, 11, 253, 2, 34, 2];
        let mut partitions = [PartitionInfo::default(); 256];
        let start = initialize_partitions(
            &list[3..], |x| *x, |_| 0, 255, |_| false, &mut partitions
        );
        assert_eq!(start, (0, 0));
        ska_byte_sort_on_partitions(&mut list, |x| *x, 255, 3, &mut partitions);
        let sorted = [1, 2, 2, 11, 34, 253];
        assert_eq!(list[3..], sorted);
    }

    /// Test that `skay_byte_sort_on_partitions` properly sorts the end of a large array of
    // bytes
    #[test]
    fn end_of_large_list_sorted_properly() {
        let size = 63543;
        let mut list = random_vec(size);
        let split = (2 * size) / 3;
        let mut sorted = Vec::from(&list[split..]);
        sorted.sort();
        let mut partitions = [PartitionInfo::default(); 256];
        let start = initialize_partitions(
            &list[split..], |x| *x, |_| 0, 255, |_| false, &mut partitions
        );
        assert_eq!(start, (0, 0));
        ska_byte_sort_on_partitions(&mut list, |x| *x, 255, split, &mut partitions);
        assert_eq!(sorted, &list[split..]);
    }

    fn string_sorter() -> impl RadixSortStrategy<&'static str> {
        RadixSortStrategyWith {
            bytes: KeyBytesWith {
                bytes: |x: &&str, b| { x.as_bytes()[b] },
                len: |x: &&str| x.len()
            },
            sort: TrySortWith(
                |arr: &mut [&str], _, _, force|
                    if force || arr.len() <= 2 { arr.sort(); true } else { false }
            )
        }
    }

    /// Test that `partition` satisfies its invariants on a list of booleans
    #[test]
    fn basic_partition_test() {
        let mut bools = [false, true, false, false, true, true, false, false, true];
        assert_eq!(partition(&mut bools, |b| *b), 4);
        assert_eq!(bools, [true, true, true, true, false, false, false, false, false]);
    }

    /// Test that `ska_sort_by_strategy` properly sorts a small array of small 64-bit integers
    #[test]
    fn small_array_of_small_u64_is_sorted_properly() {
        let mut list: Vec<u64> = vec![0, 5, 87, 3, 6, 2];
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort(&mut list);
        assert_eq!(list, sorted);
    }

    /// Test that `ska_sort_by_strategy` properly sorts an array of bytes bigger than 256
    #[test]
    fn bigger_than_byte_array_of_random_bytes_is_sorted_properly() {
        let size = 257;
        let mut list: Vec<u8> = random_vec(size);
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort(&mut list);
        for (i, (l, s)) in list.iter().zip(sorted.iter()).enumerate() {
            if l != s {
                panic!("Difference: list[{}] = {} != sorted[{}] = {}", i, l, i, s)
            }
        }
    }

    /// Test that `ska_sort_by_strategy` properly sorts an array of >256 16 bit integers
    #[test]
    fn bigger_than_byte_array_of_random_u16_is_sorted_properly() {
        let size = 257;
        let mut list: Vec<u16> = random_vec(size);
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort(&mut list);
        for (i, (l, s)) in list.iter().zip(sorted.iter()).enumerate() {
            if l != s {
                panic!("Difference: list[{}] = {} != sorted[{}] = {}", i, l, i, s)
            }
        }
    }

    /// Test that `ska_sort_by_strategy` properly sorts an array of >256 32-bit integers
    #[test]
    fn bigger_than_byte_array_of_random_u32_is_sorted_properly() {
        let size = 259;
        let mut list: Vec<u32> = random_vec(size);
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort(&mut list);
        for (i, (l, s)) in list.iter().zip(sorted.iter()).enumerate() {
            if l != s {
                panic!("Difference: list[{}] = {} != sorted[{}] = {}", i, l, i, s)
            }
        }
    }

    /// Test that `ska_sort_by_strategy` properly sorts a small array of random 64 bit integers
    #[test]
    fn small_array_of_random_u64_is_sorted_properly() {
        let size = 255;
        let mut list: Vec<u64> = random_vec(size);
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort(&mut list);
        for (i, (l, s)) in list.iter().zip(sorted.iter()).enumerate() {
            if l != s {
                panic!("Difference: list[{}] = {} != sorted[{}] = {}", i, l, i, s)
            }
        }
    }

    /// Test that `ska_sort_by_strategy` properly sorts an array of >256 random 64 bit integers
    #[test]
    fn bigger_than_byte_array_of_random_u64_is_sorted_properly() {
        let size = 257;
        let mut list: Vec<u64> = random_vec(size);
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort(&mut list);
        for (i, (l, s)) in list.iter().zip(sorted.iter()).enumerate() {
            if l != s {
                panic!("Difference: list[{}] = {} != sorted[{}] = {}", i, l, i, s)
            }
        }
    }

    /// Test that `ska_sort_by_strategy` properly sorts a medium array of random 64 bit integers
    #[test]
    fn medium_array_of_random_u64_is_sorted_properly() {
        let size = 1515;
        let mut list: Vec<u64> = random_vec(size);
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort(&mut list);
        for (i, (l, s)) in list.iter().zip(sorted.iter()).enumerate() {
            if l != s {
                panic!("Difference: list[{}] = {} != sorted[{}] = {}", i, l, i, s)
            }
        }
    }

    /// Test that `ska_sort_by_strategy` properly sorts a large array of random bytes
    #[test]
    fn large_array_of_random_bytes_is_sorted_properly() {
        let size = 1000000;
        let mut list: Vec<u8> = random_vec(size);
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort(&mut list);
        for (i, (l, s)) in list.iter().zip(sorted.iter()).enumerate() {
            if l != s {
                panic!("Difference: list[{}] = {} != sorted[{}] = {}", i, l, i, s)
            }
        }
    }

    /// Test that `ska_sort_by_strategy` properly sorts a medium array of random 16-bit integers
    #[test]
    fn medium_array_of_random_u16s_is_sorted_properly() {
        let size = 2000;
        let mut list: Vec<u16> = random_vec(size);
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort(&mut list);
        for (i, (l, s)) in list.iter().zip(sorted.iter()).enumerate() {
            if l != s {
                panic!("Difference: list[{}] = {} != sorted[{}] = {}", i, l, i, s)
            }
        }
    }

    /// Test that `ska_sort_by_strategy` properly sorts a medium array of random 16-bit integers
    #[test]
    fn large_array_of_random_u16s_is_sorted_properly() {
        let size = 200000;
        let mut list: Vec<u16> = random_vec(size);
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort(&mut list);
        for (i, (l, s)) in list.iter().zip(sorted.iter()).enumerate() {
            if l != s {
                panic!("Difference: list[{}] = {} != sorted[{}] = {}", i, l, i, s)
            }
        }
    }

    /// Test that `ska_sort_by_strategy` properly sorts a large array of random 64 bit integers
    #[test]
    fn large_array_of_random_u64_is_sorted_properly() {
        let size = 200000;
        let mut list: Vec<u64> = random_vec(size);
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort(&mut list);
        for (i, (l, s)) in list.iter().zip(sorted.iter()).enumerate() {
            if l != s {
                panic!("Difference: list[{}] = {} != sorted[{}] = {}", i, l, i, s)
            }
        }
    }

    /// Test that `ska_sort_by_strategy` properly sorts a small array of random strings
    /// with no common prefix
    #[test]
    fn small_array_of_strings_with_no_common_prefix_is_sorted_properly() {
        let mut list: Vec<&str> = vec!["a", "fgh", "e", "g", "hello"];
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort_by_strategy(
            &mut list,
            RadixSortStrategyWith {
                bytes: KeyBytesWith {
                    bytes: |x: &&str, b| { x.as_bytes()[b] },
                    len: |x: &&str| x.len()
                },
                sort: TrySortWith(
                    |arr: &mut [&str], _, _, force|
                        if force || arr.len() <= 2 { arr.sort(); true } else { false }
                )
            }
        );
        assert_eq!(list, sorted);
    }

    /// Test that `ska_sort_by_strategy` properly sorts an array of 2-bit strings
    #[test]
    fn small_array_of_bit_strings_is_sorted_properly() {
        let mut list: Vec<&str> = [
            &["ba"; 2][..],
            &["ab"; 2][..],
            &["aa"; 1][..],
            &["bb"; 1][..]
        ].concat();
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort_by_strategy(&mut list, string_sorter());
        assert_eq!(list, sorted);
    }

    /// Test that `ska_sort_by_strategy` properly sorts a small array of strings
    #[test]
    fn small_array_of_strings_is_sorted_properly() {
        let mut list: Vec<&str> = vec![
            "hello",
            "goodbye",
            "hello world",
            "hello planet",
            "goodbye world",
            "goodbye planet"
            ];
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort_by_strategy(
            &mut list,
            RadixSortStrategyWith {
                bytes: KeyBytesWith {
                    bytes: |x: &&str, b| { x.as_bytes()[b] },
                    len: |x: &&str| x.len()
                },
                sort: TrySortWith(
                    |arr: &mut [&str], _, _, force|
                        if force || arr.len() <= 2 { arr.sort(); true } else { false }
                )
            }
        );
        assert_eq!(list, sorted);
    }

    /// Test that `ska_sort_by_strategy` properly sorts a tiny array of strings
    #[test]
    fn tiny_array_of_strings_is_sorted_properly() {
        let mut list: Vec<&str> = vec![
            "a1 -- bucket 0",
            "b2 -- bucket 1",
            "a2 -- bucket 0",
            "",
            "b1 -- bucket 1"
            ];
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort_by_strategy(&mut list, string_sorter());
        assert_eq!(list, sorted);
    }

    /// Test that `ska_sort_by_strategy` properly sorts a small array of strings of
    /// increasing length
    #[test]
    fn small_array_of_increasing_prefixes_is_sorted_properly() {
        let mut list: Vec<&str> = vec![
            "",
            "a",
            "ab",
            "abc",
            "abcde",
            "abcdef"
            ];
        let mut sorted = list.clone();
        sorted.sort();
        ska_sort_by_strategy(&mut list, string_sorter());
        assert_eq!(list, sorted);
    }

}
