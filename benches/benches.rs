#[macro_use]
extern crate criterion;
extern crate rdx;

use rdx::ska_sort::ska_sort;
use criterion::{black_box, Criterion, ParameterizedBenchmark, Throughput};
use rand::distributions::{Distribution, Standard};

/// Get a random vector of n elements from the standard distribution
fn random_vec<T>(n: usize) -> Vec<T> where Standard: Distribution<T> {
    use rand::{Rng, SeedableRng};
    let mut list: Vec<T> = Vec::with_capacity(n);
    let mut rng = rand_xoshiro::Xoroshiro64StarStar::from_seed([0; 8]);
    for _ in 0..n { list.push(rng.gen()) }
    list
}

fn sort_u64_ska(n: usize) {
    let mut list: Vec<u64> = random_vec(n);
    ska_sort(&mut list);
    black_box(list);
}

fn sort_u64_unstable_std(n: usize) {
    let mut list: Vec<u64> = random_vec(n);
    list.sort_unstable();
    black_box(list);
}

fn criterion_benchmark(c: &mut Criterion) {
    c.bench(
        "sort",
        ParameterizedBenchmark::new(
            "sort-u64",
            |b, n| b.iter(|| sort_u64_ska(*n)),
            (0..6).map(|n| 500 << n).collect::<Vec<usize>>()
        )
        .throughput(|n| Throughput::Elements(*n as u64)),
    );

    c.bench(
        "sort",
        ParameterizedBenchmark::new(
            "std-sort-unstable-u64",
            |b, n| b.iter(|| sort_u64_unstable_std(*n)),
            (0..6).map(|n| 500 << n).collect::<Vec<usize>>()
        )
        .throughput(|n| Throughput::Elements(*n as u64)),
    );
}


criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
