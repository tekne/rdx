# 0.0.7

Released 2020-02-16:

* Fixed crash when sorting large arrays of variable length slices.


# 0.0.6

Released 2020-01-28:

* Fixed bug with `ska_byte_sort_on_partitions` for 2-element arrays
* Added `american_flag_byte_sort` family of functions

# 0.0.5

Released 2020-01-27:

* Further improved sorting API to be based off the `RadixSortStrategy` trait

# 0.0.4

Released 2020-01-27:

* Greatly improved sorting API with new traits, removed `Clone` bound on radix sort keys

# 0.0.3

Released 2020-01-24:

* Added `RadixSortKey` impls for `bool`, `i8`, `i16`, `i32`, `i64`, and `isize`

# 0.0.2

Released 2020-01-20:

* Added `no_std` support
* Added crate metadata, miscellaneous docs

# 0.0.1

Released 2020-01-20:

* Initial release
